# .NET Core GitLab CI

This is an experiment with static page generation using .NET Core.

## Process

- Developer machine:
    - `mkdir app`
    - `cd app`
    - `dotnet new`
    - `dotnet restore`
- GitLab Pages:
    - [Available Docker `microsoft/dotnet` image tags](https://hub.docker.com/r/microsoft/dotnet/tags/) - match your developer machine to this
    - The following goes to `.gitlab-ci.yml`

```sh
cd src
dotnet restore
dotnet publish -c Release -o .
dotnet run
```

## Sources

- [Official images for .NET Core for Linux and Windows Server 2016 Nano Server](https://hub.docker.com/r/microsoft/dotnet/)
- [Docker images for working with .NET Core and the .NET Core Tools](https://github.com/dotnet/dotnet-docker)
